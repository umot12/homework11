package homework8.Family;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Family {

    private  Human mother;
    private  Human father;
    private  List <Human>  children;
    private   Set <Pet> pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        children = new ArrayList<Human>(0);
        pet = new HashSet<>();

    }

    public Human getMother(){

        return  mother;
    }

    public  Human getFather(){
        return  father;
    }

    public List<Human> getChildren(){
        return  children;
    }

    public Set <Pet> getPet (){

        return pet;
    }

    public void setPet(Set <Pet> familyPet){

        pet = familyPet;
    }


    public  void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }

    public int countFamily(List<Human> children){

        return children.size() + 2;
    }

    public boolean deleteChild( int index){
        if(index < 0 || children == null || index > (children.size()-1)){
            return false;
        }
        int length = children.size();
        children.remove(index);
        if(length > children.size()){
            return true;
        } else return false;
    }
    public boolean deleteChild( Human child){


        int length = children.size();
        children.remove(child);
        if(length > children.size()){
            return true;
        } else return false;

    }
        public String prettyFormat(){
        String str1 = "";
        String str2 = "";
            for (int i = 0; i < children.size(); i++){
                str1 = children.get(i).getSex().equals(Human.Sex.Man)?"boy":"girl";
                str2 = str2 +str1 + children.get(i).prettyFormat();
            }
            String message =  "mother" + this.mother .prettyFormat()  + "father"+  this.father .prettyFormat() +
                    (str2.length() == 0? "" : "Child : \n"+ str2 )  +  (this.pet.stream().count() == 0? " " : this.pet.toString() ) ;
            System.out.println(message);
            return message;

        }
    @Override
    public  String toString(){
        String str = "";
        for(int i = 0;i < children.size();i++){
            str +=  children.get(i).toString() ;
        }
        String message =  this.mother .toString()  +   this.father .toString() + str +  (this.pet == null? "null" : this.pet.toString() ) ;
        System.out.println(message);
        return message;
    }

    @Override
    public int hashCode(){
        int result = this.getMother() == null?0:this.getMother().hashCode();
        result = this.getFather() == null? result : result + this.getFather().hashCode();

        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Family.class)){
            return false;
        }
        Family family = (Family) obj;
        Human familyMother = family.getMother();
        Human familyFather = family.getFather();
        if((familyMother == this.mother || familyMother.equals(this.mother))  &&
                (familyFather == this.father || familyFather.equals(this.father))) {
            return true;
        }else  return false;

    }


}

