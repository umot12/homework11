package homework8.Family;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;


public class Human {

    private    String name;
    private    String surname;
    protected long  birthDate;
    private Sex sex;
    private    byte iq;
    private    Family family;
    private   Map <DayOfWeek,String> schedule;

    public enum Sex {
        Man,NotMan;
    }

    public Human.Sex getSex() {
        return sex;
    }

    public void setSex(Human.Sex sex) {
        this.sex = sex;
    }

    public  Human(){

    }
    public  Human( String name, String surname,byte iq){
        this.name =name;
        this.surname = surname;
        this.iq = iq;


    }
    public  Human( String name, String surname,String birthDay,byte iq,Sex gender){
        this.name =name;
        this.surname = surname;
        this.iq = iq;
        this.sex = gender;
        try{
            Date date=new SimpleDateFormat("dd/MM/yyyy").parse(birthDay);
            this.birthDate = date.getTime();

        }catch(Exception e){
            System.out.println(e);

        }

    }
    public  Human( String name, String surname,byte iq,long birthDate){
        this.name =name;
        this.surname = surname;
        this.birthDate =  birthDate;
        this.iq = iq;
    }

    public  Human(String name,String surname, long birthDate,Family family){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.family = family;
    }

    public  Human(String name,String surname,long birthDate,Family family,HashMap <DayOfWeek,String> shedule){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.family = family;
        this.schedule = shedule;
    }


    public   String  getName(){
        return  name;

    }

    public void setName(String humanName){

        name = humanName;
    }

    public  String  getSurname(){
        return  surname;

    }
    public void setSurname(String humanSurname){

        surname = humanSurname;
    }

    public Family getFamily(){
        return family;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public  void setFamily(Family humanFamily){
        family = humanFamily;

    }
    public HashMap<DayOfWeek, String> getSchedule(){

        return (HashMap<DayOfWeek, String>) schedule;
    }
    public  void setSchedule (HashMap<DayOfWeek,String> humanSchedule){
        schedule = humanSchedule;
    }

    public  void greetPet ( Pet pet){

        System.out.println("Привет " + pet.getNickName() );
    }
    public   void  describePet(Pet pet){

        System.out.println( "У меня есть " + pet.getSpecies() +", ему "+ pet.getAge() + " лет, он "+ (pet.getTrickLevel() > 50? "очень хитрый":"почти не хитрый"));
    }
    public void describeAge(){
        LocalDate birthDay = Instant.ofEpochMilli(birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        System.out.println(birthDay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthDay,now);//time - age
        System.out.println("Мне " + period.getYears() + " лет " + period.getMonths() + " месяцев " + period.getDays() + " дней");
    }

    @Override
    public  String toString (){
        SimpleDateFormat form = new SimpleDateFormat("dd/MM/yyyy");
        String fomatt = form.format(birthDate);
        String message = "Human{name = " + name + " surname = " + surname +" birthDate = "+ fomatt + " iq = " + iq + " schedule = " + schedule +
                "}" ;
        System.out.println(message);
        return message;
    }
    @Override
    public int hashCode(){
        int  result = this.getSurname() == null?0:this.getSurname().hashCode();
        result = result +  iq;
        return result;
    }

    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Human.class)){
            return false;
        }
        Human human = (Human) obj;
        String humanName = human.getName();
        String humanSurname = human.getSurname();
        Family humanFamily = human.getFamily();
        if((humanName == this.name  || humanName.equals(this.name)) &&
                ( humanSurname == this.surname ||  humanSurname.equals(this.surname))  &&
                (humanFamily == this.family || humanFamily.equals(this.family))) {
            return true;
        }else  return false;

    }
            public  String prettyFormat (){
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                String formatted = format.format(birthDate);

                String message = "{name = " + name + " surname = " + surname +
                                 " birthDate = "+ formatted + " iq = " + iq +
                                 " schedule = " + schedule + "} " +"\n";

                return message;

    }

    public static void main(String[] args) {
        GregorianCalendar birthDate = new GregorianCalendar(1999,10,22);
        System.out.println(birthDate.getTimeInMillis());

        Human Oleg = new Human("Oleg","Golovach",(byte) 90, birthDate.getTimeInMillis());
        Oleg.describeAge();
        Oleg.toString();


        Human Jake =new Human("Jake","Tomson","12/03/2000",(byte) 115,Sex.Man);
        Jake.toString();


    }


}

