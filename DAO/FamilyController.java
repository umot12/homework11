package homework8.DAO;

import homework8.Family.Family;
import homework8.Family.FamilyOverflowException;
import homework8.Family.Human;
import homework8.Family.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;

    }
    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }
    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }
    public Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }
    public int count(){
        return familyService.count();
    }
    public void createNewFamily(Human mother, Human father){
        familyService.createNewFamily(mother,father);
    }
    public boolean deleteFamilyByIndex(int index){
        return familyService.deleteFamilyByIndex(index);
    }
    public Set<Pet> getPets(int index){
        return  familyService.getPets(index);
    }
    public void addPet(int index, Pet pet){
        familyService.addPet(index,pet);
    }
    public List<Family> getFamiliesBiggerThen(int size){
        return  familyService.getFamiliesBiggerThen(size);
    }
    public int   countFamiliesWithMemberNumber (int size){
        return familyService.countFamiliesWithMemberNumber(size);
    }
    public List<Family> getFamiliesLessThen(int size){
        return familyService.getFamiliesLessThen(size);
    }
    public Family  adoptChild(Family family,Human child){
        return familyService.adoptChild(family,child);
    }
    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }
    public Family bornChild(Family family,String girlName,String boyName){
        return   familyService.bornChild(family,girlName,boyName);
    }
    public void start() throws ParseException {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Menu: ");
            System.out.println("1. Insert test data");
            System.out.println("2. Show all families ");
            System.out.println("3. Find families bigger then size ");
            System.out.println("4. Find families less then size");
            System.out.println("5. Count families with size");
            System.out.println("6. Create family");
            System.out.println("7. Delete family by index ");
            System.out.println("8. Edit family ");
            System.out.println("9. Delete children older then ");
            System.out.println("10. Exit");
            System.out.println("You choice: ");

            if (!sc.hasNextInt()) {

                System.out.println("Illegal input \n enter a number from 1 to 10, depending on the menu categories");
                start();
            }
                int nummen = sc.nextInt();
            switch (nummen){
                case 1:
                try{
                    long millisInDay = 24*3600*1000;
                    LocalDate dateLoc1 =  LocalDate.of(1999,11,13);
                    long date1 = dateLoc1.toEpochDay()*millisInDay;
                    LocalDate dateLoc2 =  LocalDate.of(1909,6,10);
                    long date2 = dateLoc2.toEpochDay()*millisInDay;
                    LocalDate dateLoc3 =  LocalDate.of(2004,2,4);
                    long date3 = dateLoc3.toEpochDay()*millisInDay;
                    LocalDate childye1 = LocalDate.of(2021,10,26);
                    long chilarg12 = childye1.toEpochDay()*millisInDay;//возраст младенца
                    LocalDate childye = LocalDate.of(2015,10,26);
                    long chilarg = childye.toEpochDay()*millisInDay;//возраст биг
                    List<Family> familyList = new ArrayList<>(List.of(
                            new Family(new Human("pap1","gag1",(byte) 100,date1),new Human("mam1","gag1",(byte) 100,date3)),
                            new Family(new Human("pap2","gag2",(byte) 100,date1),new Human("mam2","gag2",(byte) 100,date1)),
                            new Family(new Human("pap3","gag3",(byte) 100,date1),new Human("mam3","gag3",(byte) 100,date1)),
                            new Family(new Human("pap4","gag4",(byte) 100,date2),new Human("mam4","gag4",(byte) 100,date3))
                    ));
                    familyList.get(0).getChildren().add(new Human("mikle","gag1","10/10/2010",(byte) 150, Human.Sex.Man));
                    familyList.get(2).getChildren().add(new Human("goha","gag1","1/1/2019",(byte) 10, Human.Sex.Man));
                    familyList.get(3).getChildren().add(new Human("Oly","gag1","10/4/2014",(byte) 150, Human.Sex.NotMan));
                    familyList.forEach(el -> familyService.addFamily(el));
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
                case 2:
                    try {
                        familyService.displayAllFamilies();

                    }  catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    catch (Exception err) {
                        err.printStackTrace();

                    }

                    break;
                case 3:

                    try{
                        System.out.println("Enter size");

                        int size = sc.nextInt();
                        familyService.getFamiliesBiggerThen(size).forEach(el -> el.prettyFormat());}
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    catch (InputMismatchException e){
                        e.printStackTrace() ;
                        System.err.println("  Введите целое число ");
                    }

                    break;

                case 4:
                    try{
                        System.out.println("Enter size");
                        int size2 = sc.nextInt();
                        List<Family> list = familyService.getFamiliesLessThen(size2);
                        list.forEach(el -> el.prettyFormat());

                    }
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    catch (InputMismatchException e) {
                        e.printStackTrace() ;
                        System.err.println("  Введите целое число ");
                    }
                    break;

                case 5:
                    try{
                        System .out.println("Enter size");
                        int size3 = sc.nextInt();
                        System.out.println(familyService.countFamiliesWithMemberNumber(size3));}
                    catch (InputMismatchException err){
                        err.printStackTrace();
                        System.err.println("  Введите целое число ");
                    }
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    break;
                case 6:
                    try{
                        sc.nextLine();
                        System.out.println("input mother name ");
                        String motherName = sc.nextLine();
                        System.out.print("");
                        System.out.println("input mother surname ");
                        String motherSurname = sc.nextLine();

                        System.out.println("input mother birth date(dd/MM/YYYY ");

                        String motherBirthDate = sc.nextLine();

                        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(motherBirthDate);
                        long birthDateMother = date.getTime();

                        System.out.println("Input father name");
                        String fatherName = sc.nextLine();
                        System.out.print("");
                        System.out.println("Input father surname");
                        String fatherSurname = sc.nextLine();

                        System.out.println("Input father birth date (dd/MM/YYYY)");

                        String fatherBirthDate = sc.nextLine();
                        Date  date2 = new SimpleDateFormat("dd/MM/yyyy").parse(fatherBirthDate);
                        long birthDateFather = date2.getTime();

                        familyService.createNewFamily(new Human(motherName, motherSurname, (byte) 110, birthDateMother), new Human(fatherName, fatherSurname, (byte) 110, birthDateFather));
                        familyService.displayAllFamilies();}
                    catch (ParseException e){
                        e.printStackTrace();
                    }catch (NullPointerException err){
                        err.printStackTrace();
                    }catch (ClassCastException err){
                        err.printStackTrace();
                    }
                    break;
                case 7:

                    try {
                        System.out.println("Please enter index of family");
                        int familyIndex = sc.nextInt();
                        System.out.println("Family you are going to delete: \n");
                        familyService.getFamilyById(familyIndex).prettyFormat();
                        familyService.deleteFamilyByIndex(familyIndex);
                        System.out.println("Family successfully deleted -there is no such family in list: \n ".toUpperCase(Locale.ROOT));
                        familyService.displayAllFamilies();
                    }
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    } catch (InputMismatchException e) {
                        System.err.println("Index is too big");
                        System.err.println("INVALID FORMAT Please try again");

                    }

                    break;

                case 8:
                    try {

                        System.out.println("Please enter index  of family");
                        int familyIndex3 = sc.nextInt();
                        Family family = familyService.getFamilyById(familyIndex3);
                        if (family.countFamily(family.getChildren()) > 6) {
                            throw new FamilyOverflowException("Family size more then " + FamilyOverflowException.familySize);
                        }
                        sc.nextLine();
                        System.out.println("1 аы- Родить ребенка");
                        System.out.println("2 - Усыновить ребенка");
                        System.out.println("3 - Вернуться в главное меню");
                        int chilmenu = sc.nextInt();
                        switch (chilmenu){
                            case 1:
                                System.out.println("Please enter name if boy");
                                String boyName = sc.nextLine();

                                System.out.println("Please enter name if girl");
                                sc.nextLine();
                                String girlName = sc.nextLine();
                                familyService.bornChild(family, girlName, boyName).prettyFormat();
                                break;
                            case 2:

                                Family family2 = familyService.getFamilyById(familyIndex3);
                                System.out.println("Please enter child name" );
                                sc.nextLine();
                                String adoptChildName = sc.nextLine();
                                System.out.println("Please enter child  surname ");
                                String adoptChildSurname = sc.nextLine();
                                System.out.println("Please enter child  birthDate(dd/MM/yyyy) ");
                                String adoptChildAge = sc.nextLine();
                                System.out.println("Please enter child  gender ");
                                String adoptChildGender = sc.nextLine();
                                System.out.println("Please enter child  iq (less then 100) ");
                                byte adoptChildIq = sc.nextByte();

                                familyService.adoptChild(family2, new Human(adoptChildName, adoptChildSurname, adoptChildAge, adoptChildIq, (adoptChildGender.equals("female") ? Human.Sex.Man : Human.Sex.NotMan))).prettyFormat();

                                break;
                            case 3:
                                continue;
                        }



                       }
                    catch (FamilyOverflowException e){
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    break;

                case 9:
                    try{
                        System.out.println("Enter child age");
                        int childAge = sc.nextInt();
                        familyService.deleteAllChildrenOlderThen(childAge);
                        familyService.displayAllFamilies();}
                    catch (InputMismatchException e){
                        e.printStackTrace();
                        System.err.println(" Введите целое число");
                    } catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    break;
                case 10:
                    System.exit(0);
                    break;

                default:
                    System.out.println("Illegal input");
            }

        }

    }
    }


