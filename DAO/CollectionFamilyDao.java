package homework8.DAO;


import homework8.Family.Family;
import homework8.Family.Human;
import homework8.Family.Pet;

import java.util.List;
import java.util.Random;
import java.util.Set;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList;

    public CollectionFamilyDao(List<Family> familyList) {
        this.familyList = familyList;

    }

    @Override
    public List<Family> getAllFamilies() {
        return this.familyList;
    }

    @Override
    public Family getFamilyByIndex(int indexOfFamily) {
        if(indexOfFamily < 0 || indexOfFamily > familyList.size()-1){
            return null;
        }
        return familyList.get(indexOfFamily);
    }

    @Override
    public boolean deleteFamily(int indexOfFamily) {
        if(indexOfFamily < 0 || indexOfFamily > familyList.size()-1) {
            return false;
        }
        familyList.remove(indexOfFamily);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
       if ( family == null){
           return false;
       }familyList.remove(family);
       return true;
    }

    @Override
    public void saveFamily(Family family) {
        familyList.add(family);
    }
}