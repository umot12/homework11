package homework8.DAO;

import homework8.Family.Family;
import homework8.Family.Human;
import homework8.Family.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {       // EDIT
       List<Family> families = familyDao.getAllFamilies();
         AtomicInteger leng = new AtomicInteger(0);
      families.stream()

                .forEach(e -> { leng.getAndIncrement();System.out.println(leng.get());e.prettyFormat();});
    }

    public Family getFamilyById(int infex) {
        return familyDao.getFamilyByIndex(infex);
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public void createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
    }
    public void addFamily (Family family){
        familyDao.saveFamily(family );
    }

    public boolean deleteFamilyByIndex(int index) {
        boolean res = familyDao.deleteFamily(index);
        return res;

    }

    public Set<Pet> getPets(int index) {
        Family ret = familyDao.getFamilyByIndex(index);
        Set <Pet> pet = ret.getPet();
        return pet;
    }
    public void addPet(int index, Pet pet) {
        Family ret = familyDao.getFamilyByIndex(index);
        Set<Pet> peti = ret.getPet();
        peti.add(pet);
    }

    public List<Family> getFamiliesBiggerThen(int count) {       // EDIT
        List<Family> families = familyDao.getAllFamilies();
        families = families.stream()
                .filter(el -> el.countFamily(el.getChildren()) > count)
                .collect(Collectors.toList());
        return families;
    }
    public int   countFamiliesWithMemberNumber (int count) {      // EDIT
        List<Family> families = familyDao.getAllFamilies();
        families = families.stream()
                .filter(el -> el.countFamily(el.getChildren()) == count)
                .collect(Collectors.toList());
        return families.size();
    }


    public List<Family> getFamiliesLessThen(int count) {     // EDIT
        List<Family> families = familyDao.getAllFamilies();
        families = families.stream()
                .filter(el -> el.countFamily(el.getChildren()) < count)
                .collect(Collectors.toList());
        return families;
    }

    public Family  adoptChild(Family family,Human child){
        List<Family> families = familyDao.getAllFamilies();
        int index = families.indexOf(family);
        Family currentFamily = families.get(index);
        currentFamily.addChild(child);
        return currentFamily;
    }

    public Family bornChild(Family family,String girlName,String boyName){
        List<Family> families = familyDao.getAllFamilies();
        int index = families.indexOf(family);
        Random random =new Random();
        boolean isBoy = random.nextBoolean();
        String  name = (isBoy? boyName : girlName);
        Family currentFamily = families.get(index);
        Calendar calendar = new GregorianCalendar();
        long birthDay = calendar.getTimeInMillis();
        Human bornChild = new Human(name,currentFamily.getFather().getSurname(),(byte) 110,birthDay );
        bornChild.setSex( (isBoy? Human.Sex.NotMan: Human.Sex.Man));
        currentFamily.addChild(bornChild);

        return currentFamily
                ;
    }

    public void deleteAllChildrenOlderThen(int age){     // EDIT
        List<Family> families = familyDao.getAllFamilies();
        families.stream().forEach(el ->{
            List<Human>famchil = el.getChildren();
            Iterator<Human> itr = famchil.iterator();

            for (; itr.hasNext(); ) {
                Period per = Period.between(Instant.ofEpochMilli(itr.next().getBirthDate())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate(), LocalDate.now());
                if (per.getYears() > age) {
                    itr.remove();
                }
            }
        });
    }

}


